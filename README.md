# Generic Express App
This app is an sample/template that i made so i could get used a little bit more to Express.JS

Feel free to use it in whatever you want too :D

## Usage
- Download Node.JS - [Download Here](https://nodejs.org/en/download/ "Node.JS Download")
- Once installed Node.JS, execute "install-dep" .sh/.bat script depending on your platform, or...
- ... execute `npm update --save` in the same folder as the script.
- Run `npm run start` for start the app in "production mode"
- Run `npm run dev` for start the app in "development mode" (a.k.a monitoring mode)