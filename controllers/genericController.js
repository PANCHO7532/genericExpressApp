const pageTitle = "GenericExpressApp";
exports.index = async(req, res) => {
    res.render("index", {
        title: pageTitle,
    });
};
exports.post = async(req, res) => {
    res.cookie("exampleExpressAppData", req.body.content, {maxAge: 3600});
    res.redirect("/page");
}
exports.page = async(req, res) => {
    res.render("page", {
        title: pageTitle,
        cookie: req.cookies.exampleExpressAppData
    });
}