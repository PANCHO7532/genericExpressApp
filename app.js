const express = require("express");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const path = require("path");
const app = express();
const routes = require("./routes");
const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || 3000;
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: true}));
app.use("/", routes());
app.use("/bootstrap", express.static(path.join(`${__dirname}/node_modules/bootstrap/dist`)));
app.use(express.static("public"));
app.set("view engine", "pug");
app.set("views", path.join(__dirname, "./views"));
app.listen(port, host, () => {
    console.log("[INFO] Server started");
});