const express = require("express");
const router = express.Router();
const genericController = require("../controllers/genericController");
module.exports = function() {
    router.get("/", genericController.index);
    router.get("/page", genericController.page);
    router.post("/post", genericController.post);
    return router;
}